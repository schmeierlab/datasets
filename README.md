# Data sources of interest
This is a non-comprehensive collection of data sources of interest to us for
quick lookup and collaborative sharing.

## Expression
- **Human/Mouse** (multiple celltypes): CAGE/FANTOM5: Promoter-level expression data / [http://fantom.gsc.riken.jp/5/datafiles/](http://fantom.gsc.riken.jp/5/datafiles/)
- **Human/Mouse** (multiple celltypes): Encode project. / [http://genome.ucsc.edu/ENCODE/downloads.html](http://genome.ucsc.edu/ENCODE/downloads.html)
- **Human** (many celltypes): GTEx / [https://gtexportal.org/home/](https://gtexportal.org/home/)

### M.tb related
- **Mouse** (macrophages):  Whole Mouse Genome Expression Array (4×44 K, Agilent). Several stimulations vs. controls. [Comprehensive analysis of TLR4-induced transcriptional responses in interleukin 4-primed mouse macrophages](http://www.sciencedirect.com/science/article/pii/S0171298510000987). DATA: [GEO](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE21895)
- **Mouse** (macrophages): Mouse Exonic Evidence-based Oligonucleotide (MEEBO) Arrays. Response to cytokines. / [Delineation of Diverse Macrophage Activation Programs in Response to Intracellular Parasites and Cytokines](http://journals.plos.org/plosntds/article?id=10.1371/journal.pntd.0000648#s2). DATA: [GEO](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE20087

## ChIP
### Histones
- **Human/Mouse** (multiple celltypes): Encode project. / [http://genome.ucsc.edu/ENCODE/downloads.html](http://genome.ucsc.edu/ENCODE/downloads.html)

### TFs
- **Human/Mouse** (multiple celltypes): Encode project / [http://genome.ucsc.edu/ENCODE/downloads.html](http://genome.ucsc.edu/ENCODE/downloads.html)
- **Human/Mouse/Rat/...** (multiuple celltypes): ChIP-Atlas / [https://github.com/inutano/chip-atlas/wiki#downloads_doc](https://github.com/inutano/chip-atlas/wiki#downloads_doc)

### Enhancers
- **Mouse** (macrophages): RNA pol II ChIP-Seq + machine learning algorithm which uses H3K4me3/H3K4me1 ratio and p300 peaks / [De Santa, F. et al. A Large Fraction of Extragenic RNA Pol II Transcription Sites Overlap Enhancers. PLoS Biol. 2010 May; 8(5): e1000384. ](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2867938/)
- **Mouse** (macrophages): P300 ChIP-Seq / [Ghisletti, S., et al. Identification and Characterization of Enhancers Controlling the Inflammatory Gene Expression Program in Macrophages. Immunity. 2010 Mar 26;32(3):317-28. ](http://www.ncbi.nlm.nih.gov/pubmed/20206554)
- **Mouse** (macrophages): H3K4me2 ChIP-Seq / [Kaikkonen, M.U., et al. Remodeling of the enhancer landscape during macrophage activation is coupled to enhancer transcription. Mol Cell. 2013, 51, 310-325](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3779836/)
- **Mouse** (macrophages): H3K4me1, H3K27ac ChIP-Seq / [Ostuni, R., et al. Latent Enhancers Activated by Stimulation in Differentiated Cells. Cell. 2013, 152, 157-171](http://www.ncbi.nlm.nih.gov/pubmed/23332752)
- **Mouse** (macrophages): Different tissues extracted macrophages. Enhancers, promoters, open chromatin / [Lavin Y, et al. Tissue-Resident Macrophage Enhancer Landscapes Are Shaped by the Local Microenvironment.Cell. 2014 Dec 4;159(6):1312-26.](http://www.ncbi.nlm.nih.gov/pubmed/25480296)
- **Human** (multiple celltypes): CAGE-based enhancers / [Andersson R, et al. An atlas of active enhancers across human cell types and tissues. Nature. 2014 Mar 27;507(7493):455-61.](http://www.ncbi.nlm.nih.gov/pubmed/24670763)

### Conformation capture
- **Mouse** (fetal liver, ESCs): Hi-C data targeted at promoter interactions. / [Schoenfelder S. et al. The pluripotent regulatory circuitry connecting promoters to their long-range interacting elements. Genome Res. 2015 Apr;25(4):582-97](http://www.ncbi.nlm.nih.gov/pubmed/25752748)
- **Mouse** (TH1): Hi-C data single cell. / [Nagano T, et a. Single-cell Hi-C for genome-wide detection of chromatin interactions that occur simultaneously in a single cell.](https://www.ncbi.nlm.nih.gov/pubmed/26540590)
- **Human** (17 primary blood cell-types, including macrophages). Promoter Hi-C. / [Javierre BM, et al. Lineage-Specific Genome Architecture Links Enhancers and Non-coding Disease Variants to Target Gene Promoters](https://www.ncbi.nlm.nih.gov/pubmed/27863249)
- **Human** (Multiple cell-types): Hi-C for TAD identification. / [Dicon et al. Topological domains in mammalian genomes identified by analysis of chromatin interactions.](https://www.ncbi.nlm.nih.gov/pubmed/27863249)
- **Human** (ESC): Hi-C for chromatin reorganization during lineage specification. / [Dixon et al. Chromatin architecture reorganization during stem cell differentiation. ](https://www.ncbi.nlm.nih.gov/pubmed/25693564)

## SNPs
- **Human** (multiple celltypes): cis-eQTLs. / [The Genotype-Tissue Expression (GTEx) pilot analysis: Multitissue gene regulation in humans. Science. 2015, 348, 6235, 648-660](http://science.sciencemag.org/content/348/6235/648)
- **Mouse** (macrophages): eQTLs./ [Orozco, et al. Unraveling inflammatory responses using systems genetics and gene-environment interactions in macrophages. Cell. 2012 Oct 26;151(3):658-70.](http://www.ncbi.nlm.nih.gov/pubmed/23101632)
- **Human** (multiple): Immunological SNPs, etc. / [Immunobase](https://www.immunobase.org/)

## Transcript assemblies
- **Human** (many celltypes): GTEx / [https://doi.org/10.1101/332825](https://doi.org/10.1101/332825)
- **Human** (many celltypes): FANTOM CAT / [http://fantom.gsc.riken.jp/cat/](http://fantom.gsc.riken.jp/cat/)